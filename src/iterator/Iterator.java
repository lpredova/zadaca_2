package iterator;

import composite.Shape;

/**
 * Created by lovro on 28/11/15.
 */
public interface Iterator {
    boolean hasNext();

    Object next();

    String currentRow();

    void validateFile();

    void validateRow();

    Shape getObject();
}
