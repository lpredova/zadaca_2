package iterator;

import chain.*;
import composite.Leaf;
import composite.Parent;
import composite.Shape;
import main.Helper;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.*;

/**
 * Created by lovro on 28/11/15.
 */
public class ShapeTreeRepository implements Container {

    public static List<String> fileConcreteData;
    public static List<Shape> shapesConcreteTree = new ArrayList<Shape>();

    //helper lists for strategies
    public static List<String> colorsArray = new ArrayList<String>();
    public static List<Integer> codesArray = new ArrayList<Integer>();

    String row;

    public static void setFileConcreteData(List<String> fileConcreteData) {
        ShapeTreeRepository.fileConcreteData = fileConcreteData;
    }

    @Override
    public Iterator getIterator() {
        return new ValidationIterator();
    }


    private class ValidationIterator implements Iterator {

        int index;

        public ValidationIterator() {

            //creating tree only on first run
            if (shapesConcreteTree.size() == 0) {
                this.createTree();
                validateFile();
            }
        }

        @Override
        public boolean hasNext() {

            if (index < fileConcreteData.size()) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {

            if (this.hasNext()) {
                return fileConcreteData.get(index++);
            }
            return null;
        }

        @Override
        public String currentRow() {
            return fileConcreteData.get(index);
        }

        @Override
        public void validateFile() {
            this.getChainOfFileValidators();
        }

        @Override
        public void validateRow() {
            this.getChainOfRowValidators();
        }

        @Override
        public Shape getObject() {
            return shapesConcreteTree.get(index);
        }


        /**
         * Chains of validators for row
         */
        private AbstractRowValidator getChainOfRowValidators() {

            AbstractRowValidator coordinatesValidator = new CoordinatesRowValidator();
            AbstractRowValidator paramsValidator = new ParamsRowValidator();

            row = this.currentRow();
            coordinatesValidator.setNextRowValidator(paramsValidator);

            if (!coordinatesValidator.validateRow(row) || !paramsValidator.validateRow(row)) {
                fileConcreteData.remove(row);
            } else {
                this.addRowToTree(row);
            }

            return coordinatesValidator;
        }


        /**
         * Chains of validators for entire file
         */
        private AbstractFileValidator getChainOfFileValidators() {

            AbstractFileValidator duplicateValidator = new DuplicateFileValidator();

            //validating duplicates
            duplicateValidator.validateFile(fileConcreteData);

            return duplicateValidator;
        }

        private void createTree() {

            //creating tree only in first run
            if (shapesConcreteTree.size() == 0) {

                String[] rootElementArray = this.findRootElement();

                //root container creates only on first run
                if (rootElementArray != null && rootElementArray.length > 0) {

                    int type = parseInt(rootElementArray[0]);
                    int code = parseInt(rootElementArray[1]);
                    int parent = parseInt(rootElementArray[2]);
                    String coordinates = rootElementArray[3];
                    String color = rootElementArray[4];

                    //root element is visible by default, and doesn't overlap with anyone
                    Shape rootElement = new Parent(type, code, parent, coordinates, color, false, true);
                    shapesConcreteTree.add(rootElement);
                    addColorToArray(color);


                } else {
                    System.out.println("Error getting tree element");
                }
            }
        }

        private String[] findRootElement() {

            List<String> file = fileConcreteData;

            for (int b = 0; b < file.size(); b++) {
                String[] elements = file.get(b).split("\\t+");
                String root = elements[0];
                String rowId = elements[1];
                String parentId = elements[2];

                if (root.equals("0") && rowId.equals(parentId)) {
                    return elements;
                }
            }
            return null;
        }

        int counter = 0;

        private void addRowToTree(String row) {

            String[] elementArray = row.split("\\t+");
            int type = parseInt(elementArray[0]);
            int code = parseInt(elementArray[1]);
            int parent = parseInt(elementArray[2]);
            String coordinates = elementArray[3];
            String color = elementArray[4];
            boolean valid = true;

            //element is root so we skip it
            if (code == parent) {
                return;
            }

            Shape parentShape = findParent(parent);
            if (parentShape == null) {
                System.out.println("Missing parent for : " + row);
                return;
            }

            //Shape is container
            if (type == 0) {

                try {
                    valid = Helper.checkOverlaps(coordinates, parentShape.getCoordinates(), row);
                    Shape element = new Parent(type, code, parent, coordinates, color, valid, true);
                    if (parentShape != null) {
                        element.addParent(parentShape);
                        parentShape.add(element);
                        addColorToArray(color);
                        addCodeToArray(code);

                    }

                } catch (Exception ignored) {
                }

            } else if (type == 1) {

                String parentCoordinates = parentShape.getCoordinates();

                try {
                    valid = Helper.checkOverlaps(coordinates, parentCoordinates, row);
                    Shape child = new Leaf(type, code, parent, coordinates, color, valid, true);
                    parentShape.add(child);
                    child.addParent(parentShape);
                    addColorToArray(color);
                    addCodeToArray(code);
                    //adding leaf children for option 2
                    parentShape.addChild(child);

                } catch (Exception e) {
                    //assume that element is valid
                    Shape child = new Leaf(type, code, parent, coordinates, color, false, true);

                    if (parentShape != null) {
                        parentShape.add(child);
                        child.addParent(parentShape);
                        addColorToArray(color);
                        addCodeToArray(code);
                        //adding leaf children for option 2
                        parentShape.addChild(child);
                    }
                }
                counter++;
            }
        }

        private Shape findParent(int parentCode) {

            for (Shape shape : shapesConcreteTree) {
                if (shape.getCode() == parentCode) {
                    return shape;

                } else {
                    return treeRecursion(shape, parentCode);
                }
            }
            return null;
        }


        private Shape treeRecursion(Shape shape, int parentCode) {

            for (Shape child : shape.getShapes()) {
                //Anchor
                if (child.getCode() == parentCode) {
                    return child;
                }

                //ok, not on this level, maybe below?
                //element is container
                if (child.getType() == 0) {
                    return this.treeRecursion(child, parentCode);
                }
            }

            return null;
        }

        private void addColorToArray(String color) {
            if (!colorsArray.contains(color)) {
                colorsArray.add(color);
            }
        }

        private void addCodeToArray(Integer code) {
            if (!codesArray.contains(code)) {
                codesArray.add(code);
            }
        }
    }
}