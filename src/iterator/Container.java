package iterator;

/**
 * Created by lovro on 28/11/15.
 */
public interface Container {
    Iterator getIterator();
}
