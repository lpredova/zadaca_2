package composite;

import main.Helper;
import main.Main;
import strategy.OperationColorsArea;
import strategy.OperationPrintState;

import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lovro on 28/11/15.
 */
public class Parent implements Shape {

    private int type;
    private int code;
    private int parent;
    private boolean valid;
    private boolean visible;
    private String coordinates;
    private String color;

    List<Shape> shapes = new ArrayList<Shape>();
    private List<Shape> childShapes = new ArrayList<Shape>();
    private List<Shape> parentShapes = new ArrayList<Shape>();


    public Parent(int type, int code, int parent, String coordinates, String color, boolean valid, boolean visible) {
        this.type = type;
        this.code = code;
        this.parent = parent;
        this.coordinates = coordinates;
        this.color = color;
        this.valid = valid;
        this.visible = visible;
    }


    @Override
    public void add(Shape shape) {
        shapes.add(shape);
    }

    @Override
    public List<Shape> getShapes() {
        return this.shapes;
    }

    @Override
    public void remove(Shape shape) {
        shapes.remove(shape);
    }

    @Override
    public Shape getChild(int i) {
        return shapes.get(i);
    }

    @Override
    public int getType() {
        return this.type;
    }

    @Override
    public int getParent() {
        return this.parent;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public boolean getValidity() {
        return this.valid;
    }

    @Override
    public boolean getVisibility() {return this.visible;}

    @Override
    public String getCoordinates() {
        return this.coordinates;
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public void setVisibility(boolean visibility) {
        this.visible = visibility;
    }

    @Override
    public void addChild(Shape shape) {
        childShapes.add(shape);
    }

    @Override
    public List<Shape> getChildren() {
        return this.childShapes;
    }

    @Override
    public void addParent(Shape shape) {
        parentShapes.add(shape);
    }

    @Override
    public List<Shape> getParents() {
        return this.parentShapes;
    }


    @Override
    public void print() {


        OperationPrintState.elementCounter++;

        if (this.getCode() == this.getParent()) {
            System.out.println("Type: Root");

        } else {
            System.out.println("Type: Container");
        }

        System.out.println("Code:" + this.getCode());
        System.out.println("Parent:" + this.getParent());
        System.out.println("Coordinates:" + this.getCoordinates());
        System.out.println("Color:" + this.getColor());
        System.out.println("Visible:" + this.getVisibility());

        if (valid) {
            System.out.println("Overlap:" + true);
        } else {
            System.out.println("Overlap:" + false);
        }
        System.out.println("------------------------------------");

        for (Shape shape : this.getShapes()) {
            shape.print();
        }
    }

    @Override
    public void colorCalculation(String color) {

        if (color.equals(this.getColor()) && !this.getValidity() && this.getVisibility()) {
            String initalCoordinates = this.getCoordinates();
            String[] coordinates = initalCoordinates.split(",");
            float localArea = 0;

            //child shape
            //square
            if (coordinates.length == 3) {

                float r = (float) Integer.parseInt(coordinates[2]);
                localArea = (float) (Math.PI * Math.pow(r, 2));
                OperationColorsArea.colorArea += localArea;
                OperationColorsArea.colorElements++;

            }
            //rectange
            else if (coordinates.length == 4) {

                float x1 = (float) Integer.parseInt(coordinates[0]);
                float y1 = (float) Integer.parseInt(coordinates[1]);

                float x2 = (float) Integer.parseInt(coordinates[2]);
                float y2 = (float) Integer.parseInt(coordinates[3]);

                float d = (float) Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));

                localArea = (x2 - x1) * (y2 - y1);

                //localArea = (float) Math.pow(d, 2) / 2;
                OperationColorsArea.colorArea += localArea;
                OperationColorsArea.colorElements++;
            }

            //polygon
            else {
                if (coordinates.length % 2 == 0) {

                    List<Integer> x = new ArrayList<Integer>();
                    List<Integer> y = new ArrayList<Integer>();

                    int i;

                    for (i = 0; i < coordinates.length; i++) {
                        if (i % 2 == 0) {
                            x.add(Integer.parseInt(coordinates[i]));
                        } else {
                            y.add(Integer.parseInt(coordinates[i]));
                        }
                    }

                    int j = coordinates.length / 2 - 1;  // The last vertex is the 'previous' one to the first

                    for (int k = 0; k < coordinates.length / 2; k++) {
                        int a = x.get(j);
                        int b = x.get(k);
                        int c = y.get(j);
                        int d = y.get(k);

                        localArea += ((a + b) * (c - d));

                        j = k;  //j is previous vertex to i
                    }

                    localArea = localArea / 2;

                    OperationColorsArea.colorArea += Math.abs(localArea);
                    OperationColorsArea.colorElements++;

                } else {
                    System.out.println("Coordinates are not odd");
                }
            }
        }

        for (Shape shape : this.getShapes()) {
            shape.colorCalculation(color);
        }
    }

    @Override
    public void changeVisibility(int elementCode, int statusCode) {

        if (elementCode == this.getCode()) {

            if (statusCode == 0) {
                this.setVisibility(false);
                System.out.println("Visibility updated!");
                return;
            } else {
                this.setVisibility(true);
                System.out.println("Visibility updated!");
                return;
            }
        }

        for (Shape shape : this.getShapes()) {
            shape.changeVisibility(elementCode, statusCode);
        }

    }

    @Override
    public void printOverlaps(Shape parent, boolean stop) {

        System.out.println("==================================\n " +
                "Parent code=" + this.getCode() + "\n");


        for (Shape parentShape : this.getShapes()) {

            int size = this.getChildren().size();

            if (stop) {
                for (int k = 0; k < size; k++) {
                    //loop for comparing elements
                    for (int j = 0; j < size; j++) {

                        Shape baseElement = this.getChildren().get(k);
                        Shape comparisonElement = this.getChildren().get(j);

                        //no comparison with itself
                        if (baseElement.getCode() == comparisonElement.getCode()) {
                            break;
                        }

                        if (Helper.checkOverlapsElements(baseElement, comparisonElement)) {
                            System.out.println("Overlaped= " + baseElement.getCode() + " : " + comparisonElement.getCode());
                        }
                    }
                }
                stop = false;
            }
            parentShape.printOverlaps(parentShape, true);
        }
    }
}
