package composite;

import sun.security.provider.SHA;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lovro on 28/11/15.
 */
public interface Shape {

    void add(Shape shape);

    List<Shape> getShapes();

    void remove(Shape shape);

    Shape getChild(int i);

    int getType();

    int getParent();

    int getCode();

    boolean getValidity();

    boolean getVisibility();

    String getCoordinates();

    String getColor();

    void setVisibility(boolean visibility);

    void addChild(Shape shape);

    List<Shape> getChildren();


    void addParent(Shape shape);

    List<Shape> getParents();

    void print();

    void colorCalculation(String color);

    void changeVisibility(int elementCode, int statusCode);

    void printOverlaps(Shape parent, boolean stop);
}
