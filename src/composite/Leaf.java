package composite;

import strategy.OperationColorsArea;
import strategy.OperationPrintState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lovro on 28/11/15.
 */
public class Leaf implements Shape {

    private int type;
    private int code;
    private int parent;
    private boolean valid;
    private boolean visible;
    private String coordinates;
    private String color;

    List<Shape> shapes = new ArrayList<Shape>();
    private List<Shape> parentShapes = new ArrayList<Shape>();


    public Leaf(int type, int code, int parent, String coordinates, String color, boolean valid, boolean visible) {
        this.type = type;
        this.code = code;
        this.parent = parent;
        this.coordinates = coordinates;
        this.color = color;
        this.valid = valid;
        this.visible = visible;
    }

    @Override
    public void add(Shape shape) {
    }

    @Override
    public List<Shape> getShapes() {
        return shapes;
    }

    @Override
    public void remove(Shape shape) {
    }

    @Override
    public Shape getChild(int i) {
        return null;
    }

    @Override
    public int getType() {
        return this.type;
    }

    @Override
    public int getParent() {
        return this.parent;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public boolean getValidity() {
        return this.valid;
    }

    @Override
    public boolean getVisibility() {
        return this.visible;
    }


    @Override
    public String getCoordinates() {
        return this.coordinates;
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public void setVisibility(boolean visibility) {
        this.visible = visibility;
    }

    @Override
    public void addChild(Shape shape) {}

    @Override
    public void addParent(Shape parent) {
        parentShapes.add(parent);
    }

    @Override
    public List<Shape> getParents() {
        return null;
    }

    @Override
    public List<Shape> getChildren() {
        return null;
    }


    @Override
    public void print() {

        OperationPrintState.elementCounter++;
        System.out.println("Type: Child");
        System.out.println("Code:" + this.getCode());
        System.out.println("Parent:" + this.getParent());
        System.out.println("Coordinates:" + this.getCoordinates());
        System.out.println("Color:" + this.getColor());
        System.out.println("Visible:" + this.getVisibility());

        if (valid) {
            System.out.println("Overlap:" + true);
        } else {
            System.out.println("Overlap:" + false);
        }
        System.out.println("------------------------------------");
    }

    @Override
    public void colorCalculation(String color) {

        if (color.equals(this.getColor()) && !this.getValidity() && this.getVisibility()) {

            String initalCoordinates = this.getCoordinates();
            String[] coordinates = initalCoordinates.split(",");
            float localArea = 0;

            //child shape
            //square
            if (coordinates.length == 3) {

                float r = (float) Integer.parseInt(coordinates[2]);
                localArea = (float) (Math.PI * Math.pow(r, 2));
                OperationColorsArea.colorArea += localArea;
                OperationColorsArea.colorElements++;

            }
            //rectange
            else if (coordinates.length == 4) {

                float x1 = (float) Integer.parseInt(coordinates[0]);
                float y1 = (float) Integer.parseInt(coordinates[1]);
                float x2 = (float) Integer.parseInt(coordinates[2]);
                float y2 = (float) Integer.parseInt(coordinates[3]);
                localArea = (x2 - x1) * (y2 - y1);

                OperationColorsArea.colorArea += localArea;
                OperationColorsArea.colorElements++;

            }
            //polygon
            else {


                //http://www.mathopenref.com/coordpolygonarea2.html
                if (coordinates.length % 2 == 0) {

                    List<Integer> x = new ArrayList<Integer>();
                    List<Integer> y = new ArrayList<Integer>();

                    int i;

                    for (i = 0; i < coordinates.length; i++) {
                        if (i % 2 == 0) {
                            x.add(Integer.parseInt(coordinates[i]));
                        } else {
                            y.add(Integer.parseInt(coordinates[i]));
                        }
                    }

                    int j = coordinates.length / 2 - 1;  // The last vertex is the 'previous' one to the first

                    for (int k = 0; k < coordinates.length / 2; k++) {
                        int a = x.get(j);
                        int b = x.get(k);
                        int c = y.get(j);
                        int d = y.get(k);

                        localArea += ((a + b) * (c - d));
                        j = k;  //j is previous vertex to i
                    }

                    localArea = localArea / 2;

                    OperationColorsArea.colorArea += Math.abs(localArea);
                    OperationColorsArea.colorElements++;

                } else {
                    System.out.println("Coordinates are not odd");
                }

            }
        }
    }

    @Override
    public void changeVisibility(int elementCode, int statusCode) {
        if (elementCode == this.getCode()) {

            if (statusCode == 0) {
                this.setVisibility(false);
                System.out.println("Visibility updated!");

            } else {
                this.setVisibility(true);
                System.out.println("Visibility updated!");
            }
        }
    }

    @Override
    public void printOverlaps(Shape parent, boolean stop) {}

}
