package singleton;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Parent class which provides basic file reading operations
 */
abstract class AppFileReader {

    public static List<String> fileContent;


    public boolean fileExists(String fileName) {

        File f = new File(fileName);

        return f.exists() && !f.isDirectory();
    }


    public List<String> readFile(String fileName) throws IOException {

        fileContent = new ArrayList<String>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        String line = null;

        while ((line = bufferedReader.readLine()) != null) {

            if (line == null) {
                break;
            } else {
                fileContent.add(line);
            }
        }

        bufferedReader.close();
        return fileContent;
    }


    public void printFile() {
        for (String line : fileContent) {
            System.out.println(line);
        }
    }

}
