package singleton;

import java.util.ArrayList;
import java.util.List;

/**
 * File reader which handles basic reading from input file provided.
 */
public class ConcreteFileReader extends AppFileReader {

    private static volatile ConcreteFileReader instance = null;
    public static List<String> data = new ArrayList<String>();


    protected ConcreteFileReader() {

    }

    public static ConcreteFileReader getInstance() {
        if (instance == null) {
            instance = new ConcreteFileReader();
        }

        return instance;
    }
}
