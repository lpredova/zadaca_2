package strategy;

import composite.Shape;
import iterator.ShapeTreeRepository;

/**
 * Created by lovro on 30/11/15.
 */
public class OperationPrintOverlaps implements Strategy {
    @Override
    public void doOperation() {

        for (Shape shape : ShapeTreeRepository.shapesConcreteTree) {
            shape.printOverlaps(shape, true);
        }

        System.out.println("\n\n");
    }
}
