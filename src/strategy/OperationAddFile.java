package strategy;

import iterator.Iterator;
import iterator.ShapeTreeRepository;
import main.Helper;
import main.Main;
import singleton.ConcreteFileReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lovro on 30/11/15.
 */
public class OperationAddFile implements Strategy {

    String fileName;
    public Matcher m;
    public static List<String> additionalFileData;


    @Override
    public void doOperation() {

        if (readFile()) {
            ConcreteFileReader concreteFileReader = ConcreteFileReader.getInstance();
            if (concreteFileReader.fileExists(m.group(1))) {

                try {
                    additionalFileData = concreteFileReader.readFile(fileName);
                    System.out.println(additionalFileData);
                    System.out.println("Adding new file!");



                    ShapeTreeRepository.setFileConcreteData(additionalFileData);

                    for (Iterator iterator = Main.shapeTree.getIterator(); iterator.hasNext(); ) {
                        iterator.validateRow();
                        iterator.next();
                    }

                    Context contextPrintNewTree = new Context(new OperationPrintState());
                    contextPrintNewTree.executeStrategy();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        } else {
            Helper.invalidFileErr();
        }
    }

    private boolean readFile() {

        System.out.println("Enter path to new file:");
        Helper.printNewLine();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        try {
            fileName = br.readLine();
        } catch (IOException ignored) {
        }

        String filePattern = "(.*\\.txt$)";

        /**
         * Evaluating input params format
         */
        StringBuilder sb = new StringBuilder();

        Pattern pattern = Pattern.compile(filePattern);
        sb.append(fileName).append(" ");

        String p = sb.toString().trim();
        m = pattern.matcher(p);
        return m.matches();
    }

}
