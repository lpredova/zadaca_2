package strategy;

import composite.Shape;
import iterator.ShapeTreeRepository;
import main.Helper;

/**
 * Created by lovro on 30/11/15.
 */
public class OperationColorsArea implements Strategy {

    public static float colorArea;
    public static int colorElements;

    @Override
    public void doOperation() {

        System.out.println("Calculating colors area");
        Helper.printNewLine();

        for (String color : ShapeTreeRepository.colorsArray) {
            colorArea = 0;
            colorElements = 0;

            for (Shape shape : ShapeTreeRepository.shapesConcreteTree) {
                shape.colorCalculation(color);
            }

            System.out.println(color + " = " + colorArea + "\t in " + colorElements + "\t elements");
        }
        Helper.printNewLine();
    }
}
