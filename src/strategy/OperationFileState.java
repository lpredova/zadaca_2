package strategy;

import main.Helper;
import main.Main;

import java.util.List;

/**
 * Created by lovro on 02/12/15.
 */
public class OperationFileState implements Strategy {
    @Override
    public void doOperation() {

        Main.originator.getStateFromMemento(Main.careTaker.get(0));
        List<String> initialFileState = Main.originator.getState();

        System.out.println("Initial file state:");
        Helper.printNewLine();

        for (String row : initialFileState) {
            System.out.println(row);
        }

        Helper.printNewLine();

    }
}
