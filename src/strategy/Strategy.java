package strategy;

/**
 * Created by lovro on 30/11/15.
 */
public interface Strategy {
    public void doOperation();
}
