package strategy;

import composite.Shape;
import iterator.ShapeTreeRepository;
import main.Helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by lovro on 30/11/15.
 */
public class OperationChangeStatus implements Strategy {

    public int elementCode;
    public int statusCode;


    @Override
    public void doOperation() {

        System.out.println("Changing status");
        Helper.printNewLine();

        Context contextPrintNewTree = new Context(new OperationPrintState());
        contextPrintNewTree.executeStrategy();

        boolean exists = true;
        BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

        do {
            try {
                System.out.println("Enter code of element you want to edit:");
                elementCode = Integer.parseInt(br1.readLine());
                exists = checkCodeExists(elementCode);

            } catch (IOException ex) {
                System.err.println("Please select option  1-4.");
            }

        } while (!exists);


        do {
            System.out.println("Set element status: (1 - visible, 0 - hidden):");
            try {
                statusCode = Integer.parseInt(br2.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }

        } while (statusCode != 0 && statusCode != 1);


        for (Shape shape : ShapeTreeRepository.shapesConcreteTree) {
            shape.changeVisibility(elementCode, statusCode);
        }

        contextPrintNewTree.executeStrategy();
    }

    private boolean checkCodeExists(int code) {
        if (ShapeTreeRepository.codesArray.contains(code)) {
            return true;
        }
        return false;
    }
}
