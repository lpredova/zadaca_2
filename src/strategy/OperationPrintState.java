package strategy;

import composite.Shape;
import iterator.ShapeTreeRepository;
import main.Helper;

/**
 * Created by lovro on 30/11/15.
 */
public class OperationPrintState implements Strategy {

    public static int elementCounter = 0;

    @Override
    public void doOperation() {

        elementCounter = 0;
        for (Shape shape : ShapeTreeRepository.shapesConcreteTree) {
            shape.print();
        }
        Helper.printNewLine();
        System.out.println("Total elements: " + elementCounter);
        Helper.printNewLine();

    }
}
