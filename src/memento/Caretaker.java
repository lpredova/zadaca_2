package memento;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lovro on 02/12/15.
 */
public class Caretaker {

    private List<Memento> mementoList = new ArrayList<Memento>();

    public void add(Memento state){
        mementoList.add(state);
    }

    public Memento get(int index){
        return mementoList.get(index);
    }
}