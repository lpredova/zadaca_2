package memento;

import java.util.List;

/**
 * Created by lovro on 02/12/15.
 */
public class Memento {

    private List<String> state;

    public Memento(List<String> state) {
        this.state = state;
    }

    public List<String> getState() {
        return state;
    }
}