package memento;

import java.util.List;

/**
 * Created by lovro on 02/12/15.
 */
public class Originator {
    private List<String> state;

    public void setState(List<String> state) {
        this.state = state;
    }

    public List<String> getState() {
        return state;
    }

    public Memento saveStateToMemento() {
        return new Memento(state);
    }

    public void getStateFromMemento(Memento Memento) {
        state = Memento.getState();
    }
}
