package template;

/**
 * Created by lovro on 28/11/15.
 */
public class ParamsError extends ErrorTemplate {

    @Override
    public void addError(String error) {
        if (error != null) {
            errorData += "\nERROR : " + error + "\n";
        }
    }

    @Override
    public void printErrors() {
        System.out.println(errorData);
    }
}
