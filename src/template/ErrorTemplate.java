package template;

/**
 * Class that defines error handling options
 */
public abstract class ErrorTemplate {

    public String errorData = "\n";

    public abstract void addError(String error);

    public abstract void printErrors();
}
