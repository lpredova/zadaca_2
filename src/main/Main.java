package main;

import iterator.Iterator;
import iterator.ShapeTreeRepository;
import memento.Caretaker;
import memento.Originator;
import singleton.ConcreteFileReader;
import strategy.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static List<String> fileData;
    public static ShapeTreeRepository shapeTree;
    public static Originator originator = new Originator();
    public static Caretaker careTaker = new Caretaker();
    public static Context context;

    public static void main(String[] args) {

        String filePattern = "(.*\\.txt$)";
        boolean status;


        /**
         * Evaluating input params format
         */
        StringBuilder sb = new StringBuilder();
        Pattern pattern = Pattern.compile(filePattern);

        for (String arg : args) {
            sb.append(arg).append(" ");
        }

        String p = sb.toString().trim();
        Matcher m = pattern.matcher(p);
        status = m.matches();


        if (status) {
            /**
             * Reading file on initial run
             */

            ConcreteFileReader concreteFileReader = ConcreteFileReader.getInstance();
            if (concreteFileReader.fileExists(m.group(1))) {
                System.out.println("Reading file:  " + m.group(1) + " successful! ");

                try {
                    fileData = concreteFileReader.readFile(m.group(1));
                    concreteFileReader.printFile();
                    Helper.printNewLine();


                    //Memento ops
                    originator.setState(concreteFileReader.readFile(m.group(1)));
                    careTaker.add(originator.saveStateToMemento());


                    shapeTree = new ShapeTreeRepository();
                    ShapeTreeRepository.setFileConcreteData(fileData);

                    System.out.println("\nSize:" + fileData.size());

                    //validating data and building tree
                    for (Iterator iterator = shapeTree.getIterator(); iterator.hasNext(); ) {
                        iterator.validateRow();
                        iterator.next();
                    }

                    Helper.printClearFile();
                    Helper.printNewLine();
                    menuOperations();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Helper.invalidFileErr();
            }
        } else {
            Helper.invalidFileErr();
        }
    }

    private static void menuOperations() {

        System.out.println("\nTree has : " + fileData.size() + " nodes");
        int selection = 1;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        do {
            Helper.printMenu();
            try {
                selection = Integer.parseInt(br.readLine());
            } catch (IOException ex) {
                System.err.println("Please select option  1-4.");
            }

            switch (selection) {
                case 1:
                    //print state
                    context = new Context(new OperationPrintState());
                    context.executeStrategy();
                    break;

                case 2:
                    //printing overlapped elements
                    context = new Context(new OperationPrintOverlaps());
                    context.executeStrategy();
                    break;

                case 3:
                    //update elements
                    context = new Context(new OperationChangeStatus());
                    context.executeStrategy();
                    break;
                case 4:
                    //calculating areas by color
                    context = new Context(new OperationColorsArea());
                    context.executeStrategy();
                    break;
                case 5:
                    //calculating areas by color
                    context = new Context(new OperationAddFile());
                    context.executeStrategy();
                    break;
                case 6:
                    //getting read file state from memento
                    context = new Context(new OperationFileState());
                    context.executeStrategy();
                    break;
                case 0:
                    System.out.println("Exiting...");
                    break;
            }
        } while (selection != 0);
    }

}
