package main;

import composite.Shape;
import iterator.ShapeTreeRepository;

import java.awt.*;
import java.awt.geom.*;

/**
 * Created by lovro on 28/11/15.
 */
public class Helper {

    public static void printMenu() {
        System.out.println("============================================");
        System.out.println("|                MENU                      |");
        System.out.println("============================================");
        System.out.println("|                                          |");
        System.out.println("|        1. Check state                    |");
        System.out.println("|        2. Print overlapped elements      |");
        System.out.println("|        3. Update elements                |");
        System.out.println("|        4. Calculate colors of areas      |");
        System.out.println("|        5. Read additional file           |");
        System.out.println("|        6. Get initial file state         |");
        System.out.println("|        0. Exit                           |");
        System.out.println("============================================");
        System.out.println(" Select option : ");
        printNewLine();
    }

    public static void printNewLine() {
        System.out.println("\n");
    }

    public static void invalidFileErr() {
        System.out.println("Invalid file format");
    }

    public static void printClearFile() {

        for (String line : ShapeTreeRepository.fileConcreteData) {
            System.out.println(line);
        }
    }


    public static boolean checkOverlaps(String elementCoords, String parentCoords, String row) {

        return Helper.getCollisionAxis(elementCoords, parentCoords, true, row);
    }

    private static String[] getArrayFromCoordinates(String element) {

        return element.split(",");
    }

    public static boolean getCollisionAxis(String elementCoords, String parentCoords, boolean overlaps, String row) {
        String[] parent = Helper.getArrayFromCoordinates(parentCoords);
        String[] child = Helper.getArrayFromCoordinates(elementCoords);

        boolean status = false;

        if (child.length == 3) {
            if (((Integer.parseInt(child[0]) - Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) < Integer.parseInt(parent[0]))
                    && ((Integer.parseInt(child[0]) + Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) > Integer.parseInt(parent[0]))
                    && ((Integer.parseInt(child[0]) + Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) <= Integer.parseInt(parent[2]))
                    || ((Integer.parseInt(child[0]) + Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) > Integer.parseInt(parent[2]))
                    && ((Integer.parseInt(child[0]) - Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) < Integer.parseInt(parent[2]))
                    && ((Integer.parseInt(child[0]) - Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) <= Integer.parseInt(parent[0]))) {

                System.out.println("\nOverlap on X in row: " + row + "\n");
                status = true;
            }

            if (((Integer.parseInt(child[1]) - Integer.parseInt(child[2]) + Integer.parseInt(parent[1])) < Integer.parseInt(parent[1]))
                    && ((Integer.parseInt(child[1]) + Integer.parseInt(child[2]) + Integer.parseInt(parent[1])) > Integer.parseInt(parent[1]))
                    && ((Integer.parseInt(child[1]) + Integer.parseInt(child[2]) + Integer.parseInt(parent[1])) <= Integer.parseInt(parent[3]))
                    || ((Integer.parseInt(child[1]) + Integer.parseInt(child[2]) + Integer.parseInt(parent[1])) > Integer.parseInt(parent[3]))
                    && ((Integer.parseInt(child[1]) - Integer.parseInt(child[2]) + Integer.parseInt(parent[1])) < Integer.parseInt(parent[3]))
                    && ((Integer.parseInt(child[1]) - Integer.parseInt(child[2]) + Integer.parseInt(parent[1])) >= Integer.parseInt(parent[1]))) {

                System.out.println("\nOverlap on Y in row: " + row + "\n");
                status = true;

            }
        } else if (child.length == 4) {

            if ((((Integer.parseInt(child[0]) + Integer.parseInt(parent[0])) < Integer.parseInt(parent[0]))
                    && ((Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) > Integer.parseInt(parent[0]))
                    && ((Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) <= Integer.parseInt(parent[2]))
                    || ((Integer.parseInt(child[0]) + Integer.parseInt(parent[0])) >= Integer.parseInt(parent[0]))
                    && ((Integer.parseInt(child[0]) + Integer.parseInt(parent[0])) < Integer.parseInt(parent[2]))
                    && ((Integer.parseInt(child[2]) + Integer.parseInt(parent[0])) > Integer.parseInt(parent[2])))) {
                System.out.println("\nOverlap on X in row: " + row + "\n");
                status = true;

            }
            if (((Integer.parseInt(child[1]) + Integer.parseInt(parent[1])) < Integer.parseInt(parent[1]))
                    && ((Integer.parseInt(child[3]) + Integer.parseInt(parent[1])) > Integer.parseInt(parent[1]))
                    && ((Integer.parseInt(child[3]) + Integer.parseInt(parent[1])) <= Integer.parseInt(parent[3]))
                    || ((Integer.parseInt(child[1]) + Integer.parseInt(parent[1])) >= Integer.parseInt(parent[1]))
                    && ((Integer.parseInt(child[1]) + Integer.parseInt(parent[1])) < Integer.parseInt(parent[3]))
                    && ((Integer.parseInt(child[3]) + Integer.parseInt(parent[1])) > Integer.parseInt(parent[3]))) {
                System.out.println("\nOverlap on Y in row: " + row + "\n");
                status = true;

            }
        } else {

            for (int i = 0; i < (child.length / 2); i++) {
                if (((Integer.parseInt(child[2 * i]) + Integer.parseInt(child[0])) > Integer.parseInt(parent[2]))) {

                    System.out.println("\nOverlap on X in row: " + row + "\n");
                    status = true;
                }
            }

            for (int i = 0; i < (child.length / 2); i++) {
                if (((Integer.parseInt(child[(2 * i) + 1]) + Integer.parseInt(child[1])) > Integer.parseInt(parent[3]))) {

                    System.out.println("\nOverlap on Y in row: " + row + "\n");
                    status = true;
                }
            }
        }
        return status;
    }


    public static boolean checkOverlapsElements(Shape first, Shape second) {

        String[] parent = Helper.getArrayFromCoordinates(first.getCoordinates());
        String[] child = Helper.getArrayFromCoordinates(second.getCoordinates());

        java.awt.Shape childArea;
        java.awt.Shape parentArea;

        //child shape
        if (child.length == 3) {

            float x = (float) Integer.parseInt(child[0]);
            float y = (float) Integer.parseInt(child[1]);
            float z = (float) Integer.parseInt(child[2]);
            childArea = new Ellipse2D.Float(x, y, z, z);

        } else if (child.length == 4) {

            int[] x = new int[2];
            int[] y = new int[2];

            x[0] = Integer.parseInt(child[0]);
            y[0] = Integer.parseInt(child[1]);
            x[1] = Integer.parseInt(child[2]);
            y[1] = Integer.parseInt(child[3]);
            childArea = new Polygon(x, y, 2);

        } else {

            int[] x = new int[child.length / 2];
            int[] y = new int[child.length / 2];
            int i;

            for (i = 0; i < (child.length - 1); i += 2) {

                x[i / 2] = Integer.parseInt(child[i]);

                int place = ((int) Math.ceil((i + 1) / 2));
                y[place] = Integer.parseInt(child[i + 1]);
            }

            childArea = new Polygon(x, y, child.length / 2);
        }

        //parent shape
        if (parent.length == 3) {

            float x = (float) Integer.parseInt(parent[0]);
            float y = (float) Integer.parseInt(parent[1]);
            float z = (float) Integer.parseInt(parent[2]);
            parentArea = new Ellipse2D.Float(x, y, z, z);

        } else if (parent.length == 4) {

            int[] x = new int[2];
            int[] y = new int[2];

            x[0] = Integer.parseInt(parent[0]);
            y[0] = Integer.parseInt(parent[1]);
            x[1] = Integer.parseInt(parent[2]);
            y[1] = Integer.parseInt(parent[3]);
            parentArea = new Polygon(x, y, 2);
        } else {

            int[] x = new int[parent.length / 2];
            int[] y = new int[parent.length / 2];
            int i;

            for (i = 0; i < (parent.length - 1); i += 2) {

                x[i / 2] = Integer.parseInt(parent[i]);

                int place = ((int) Math.ceil((i + 1) / 2));
                y[place] = Integer.parseInt(parent[i + 1]);
            }

            parentArea = new Polygon(x, y, parent.length / 2);
        }

        return collidesWith(parentArea, childArea);
    }

    public static boolean collidesWith(java.awt.Shape first, java.awt.Shape other) {
        return first.getBounds2D().intersects(other.getBounds2D());
    }
}