package chain;


/**
 * Created by lovro on 28/11/15.
 */
public abstract class AbstractRowValidator extends AbstractValidator {

    public void setNextRowValidator(AbstractRowValidator nextValidator) {
        this.nextValidator = nextValidator;
    }

    public abstract boolean validateRow(String row);

}
