package chain;

import iterator.ShapeTreeRepository;
import template.DuplicateError;

import java.util.List;

/**
 * Created by lovro on 28/11/15.
 */
public class DuplicateFileValidator extends AbstractFileValidator {

    DuplicateError duplicateError = new DuplicateError();


    @Override
    public boolean validateFile(List<String> file) {

        boolean valid = true;
        boolean rootExists;


        // initial file reading
        if (ShapeTreeRepository.shapesConcreteTree.get(0).getShapes().size() == 0) {
            rootExists = false;
            for (int b = 0; b < file.size(); b++) {
                String[] tokens = file.get(b).split("\\t+");
                String root = tokens[0];
                String rowId = tokens[1];
                String parentId = tokens[2];

                //running only on first
                if (root.equals("0") && rowId.equals(parentId)) {
                    if (!rootExists) {
                        rootExists = true;
                    } else {
                        duplicateError.addError("There is more than one root element in row:\n" + file.get(b) + "\n");
                        file.remove(file.get(b));
                        valid = false;
                    }
                }
            }
            if (!rootExists) {
                duplicateError.addError("There is no root element");
                valid = false;
            }
        }


        //duplicate node code
        for (int i = 0; i < file.size(); i++) {

            String[] tokens = file.get(i).split("\\t+");
            String code = tokens[1];

            for (int k = i + 1; k < file.size(); k++) {

                String[] token = file.get(k).split("\\t+");
                String newCode = token[1];

                if (newCode.equals(code)) {
                    duplicateError.addError("There are elements with same code: \n" + file.get(k) + "\n" + file.get(i));
                    file.remove(file.get(k));
                }
            }
        }

        //parent node existing - initial read
        for (int i = 0; i < file.size(); i++) {

            String[] tokens = file.get(i).split("\\t+");
            String parentNode = tokens[2];

            boolean parentNotExisting = false;

            for (int k = 0; k < file.size(); k++) {

                //skipping checking element with himself and root element
                if (i == k) {
                    break;
                }

                String[] token = file.get(k).split("\\t+");
                String newCode = token[1];

                if (newCode.equals(parentNode)) {
                    parentNotExisting = true;
                }
            }
            if (!parentNotExisting && !tokens[0].equals("0")) {
                duplicateError.addError("\nParent element does not exist for row: \n" + file.get(i));
                file.remove(file.get(i));
            }
        }

        if (!valid) {
            this.printErrorMsg();
            return false;
        }

        return true;
    }

    @Override
    public void printErrorMsg() {
        duplicateError.printErrors();
    }


}
