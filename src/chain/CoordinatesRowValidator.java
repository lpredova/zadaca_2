package chain;

import template.CoordinatesError;

/**
 * Created by lovro on 28/11/15.
 */
public class CoordinatesRowValidator extends AbstractRowValidator {

    CoordinatesError coordinatesError = new CoordinatesError();

    @Override
    public boolean validateRow(String row) {

        boolean valid = true;

        String[] tokens = row.split("\\t+");
        String[] coordinates = tokens[3].split(",");

        //string has less than 3 coords
        if (coordinates.length < 3) {
            coordinatesError.addError("Less than 3 params for coordinates in row:\n" + row + "\n");
            valid = false;
        }

        //number of coordinates is wrong, not even if there are more than 4 coords
        if (coordinates.length > 4 && (coordinates.length % 2 != 0)) {
            coordinatesError.addError("Wrong number of coords for coordinates in row:\n" + row + "\n");
            valid = false;
        }

        //same params
        if (allElementsTheSame(coordinates)) {
            coordinatesError.addError("All coordinates are the same in row:\n" + row + "\n");
            valid = false;
        }

        //positive numbers
        if (!allElementsPositive(coordinates)) {
            coordinatesError.addError("All coordinates must be positive numbers in range 0-9999 in row:\n" + row + "\n");
            valid = false;
        }

        //numbers in range
        if (!allElementsInRange(coordinates)) {
            coordinatesError.addError("All coordinates must be numbers from 0-9999 in row: \n" + row + "\n");
            valid = false;
        }

        if (!valid) {
            this.printErrorMsg();
            return false;
        }
        return true;
    }

    @Override
    public void printErrorMsg() {
        coordinatesError.printErrors();
    }

    public static boolean allElementsTheSame(String[] array) {

        boolean flag = true;
        String first = array[0];

        for (int i = 0; i < array.length && flag; i++) {

            if (!array[i].equals(first)) {
                flag = false;
            }
        }
        return flag;
    }

    public static boolean allElementsPositive(String[] array) {

        boolean flag = true;

        for (int i = 0; i < array.length && flag; i++) {
            int element = Integer.parseInt(array[i]);

            if (element < 0) {
                flag = false;
            }
        }
        return flag;
    }

    public static boolean allElementsInRange(String[] array) {

        boolean flag = true;

        for (int i = 0; i < array.length && flag; i++) {
            int element = Integer.parseInt(array[i]);

            if ((element < 0) || (element > 9999)) {
                flag = false;

            }
        }
        return flag;
    }
}
