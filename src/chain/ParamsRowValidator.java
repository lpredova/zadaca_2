package chain;

import iterator.ShapeTreeRepository;
import template.ParamsError;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lovro on 28/11/15.
 */
public class ParamsRowValidator extends AbstractRowValidator {


    ParamsError coordinatesError = new ParamsError();

    @Override
    public boolean validateRow(String row) {

        boolean valid = true;

        String[] tokens = row.split("\\t+");

        //string has less than 3 coords
        if (tokens.length != 5) {
            coordinatesError.addError("Wrong number of params!");
            valid = false;
        }

        //same params
        int index = 0;
        for (String token : tokens) {

            if (index == 4) {
                Pattern colorPattern = Pattern.compile("^#(?:[0-9a-fA-F]{3}){1,2}$");
                Matcher m = colorPattern.matcher(token);
                boolean isColor = m.matches();
                if (!isColor) {
                    coordinatesError.addError("Wrong color pattern in row: " + row);
                    valid = false;
                }
            } else if (index != 3) {
                if (!isNumeric(token)) {
                    coordinatesError.addError("Numeric params are required for element in row: " + row);
                    valid = false;
                }
            }
            //validating color
            else {
                String[] coordinates = tokens[3].split(",");
                for (String coordinate : coordinates) {
                    if (!isNumeric(coordinate)) {
                        coordinatesError.addError("Numeric params are required for coordinates!");
                        valid = false;
                    }
                }
            }
            index++;
        }

        //element wants to be root!?
        //its not root if its different than first element in tree
        int rootCode = ShapeTreeRepository.shapesConcreteTree.get(0).getCode();
        int rowCode = Integer.parseInt(tokens[1]);
        if (tokens[1].equals(tokens[2]) &&  rootCode!= rowCode) {
            coordinatesError.addError("Element cannot be root! in row: " + row);
            valid = false;
        }

        if (!valid) {
            this.printErrorMsg();
            return false;
        }
        return true;
    }

    @Override
    public void printErrorMsg() {
        coordinatesError.printErrors();
    }


    public boolean isNumeric(String s) {
        return s.matches("[-+]?\\d*\\.?\\d+");
    }
}
