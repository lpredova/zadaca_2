package chain;

import template.ErrorTemplate;

/**
 * Created by lovro on 28/11/15.
 */
abstract class AbstractValidator {

    protected AbstractValidator nextValidator;
    protected ErrorTemplate error;

    public abstract void printErrorMsg();
}
