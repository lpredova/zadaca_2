package chain;

import java.util.List;

/**
 * Created by lovro on 28/11/15.
 */
public abstract class AbstractFileValidator extends AbstractValidator {

    public void setNextFileValidator(AbstractFileValidator nextValidator) {
        this.nextValidator = nextValidator;
    }

    public abstract boolean validateFile(List<String> file);
}
